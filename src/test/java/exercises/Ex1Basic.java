package exercises;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class Ex1Basic {


    /*******************************************************
     * Send a GET request to "  http://localhost:3000/users"
     * and check that the response has HTTP status code 200
     ******************************************************/
    @Test
    public void testGetUsers() {
        Response response = RestAssured.get("http://localhost:3000/users");
        int code= response.getStatusCode();

        System.out.println("My status code is " + code);
        assertEquals(code, 200);

       /* given().when().get("http://localhost:3000/users").then().assertThat().statusCode(200);*/

    }


    /*******************************************************
     * Send a GET request to ""  http://localhost:3000/incorrect"
     * and check that the answer has HTTP status code 404
     ******************************************************/
    @Test
    public void testIncorrectRequest() {
        Response response = RestAssured.get("http://localhost:3000/incorrect");
        int code = response.getStatusCode();
        assertEquals(code, 404);

        /* given().when().get("http://localhost:3000/incorrect").then().assertThat().statusCode(404);*/
    }



    /*******************************************************
     * Retrieve users collection and assert that
     * the returned content type is "application/json"
     * and the collection has 10 items
     ******************************************************/
    @Test
    public void testUsersSize(){
           given().when().get("http://localhost:3000/users")
                   .then().assertThat().contentType("application/json")
                   .body("size", equalTo(10));

            }


    /*******************************************************
     * Check that the users collection contains items
     * having company name "Johns Group"
     ******************************************************/
    @Test
    public void testUsersWithCompany() {

        given().when().get("http://localhost:3000/users").then().assertThat().body("company name", is("Johns Group"));

    }

}
