package exercises;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItem;

public class Ex2Parameters {

    @BeforeAll
    public static void setup(){
        RestAssured.baseURI="http://localhost";
        RestAssured.port= 3000;
    }
    /*******************************************************
     * Check that the user with ID 1 has 15 todos
     * Use pathParam for creating the request
     ******************************************************/
    @Test
    public void testTodosForUser() {
        given().log().all()
                .queryParam("userId", 1)
                .when().get("users")
                .then().log().all().assertThat().statusCode(200);


    }


    /*******************************************************
     * Check that the user with ID 1 has a post called "qui est esse"
     * Use queryParam for creating the request
     ******************************************************/
    @Test
    public void testPostForUser() {

        given().log().all()
                .queryParam("userId", 1)
                .when().get("users")
                .then().log().all().assertThat().body("post", hasItem("qui est esse"));
    }
}