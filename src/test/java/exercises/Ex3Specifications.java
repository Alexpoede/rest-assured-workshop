package exercises;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItem;

public class Ex3Specifications {
    private static RequestSpecification requestSpec;
    private static ResponseSpecification responseSpec;



    /*************************************************************
     * Create request and response specifications using builders
     * Request specification should include our base URI and
     * a base path that points to comments collection
     * Response specification should expect status code 200
     ************************************************************/
    @BeforeAll
    public static void createSpecifications() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri("http://localhost:3000");
        builder.setBasePath("/comments");
        requestSpec = builder.build();
        responseSpec= new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();
    }

   @Test
           public void responseCode() {

       given().
               spec(requestSpec).when().get("/comments")
               .then().statusCode(200);

   }

    /**********************************************************
     * Delete the comment with ID 1 (use both specifications)
     *********************************************************/
    @Test
    public void testDeleteComment() {

       given()
                .spec(requestSpec)
                .pathParam("Id", 1)
                .when().
                log().all()
                .delete("/{id}")
                .then()
                .spec(responseSpec);


    }



    /**********************************************************
     * Update the comment with ID 2
     * (use both specifications)
     *********************************************************/
    @Test
    public void testAddComment() {
        given()
                .spec(requestSpec)
                .pathParam("id", 2)
                .when()
                .body(" {\n" +
                        "    \"postId\": 1,\n" +
                        "    \"id\": 2,\n" +
                        "    \"name\": \"id labore ex et quam laborum\",\n" +
                        "    \"email\": \"Eliseo@gardner.biz\",\n" +
                        "    \"body\": \"laudantium enim quasi est quidem magnam voluptate ipsam eos12345\"\n" +
                        "  }")
                .header("content-type", "application/json")
                .put("/{id}")
                .then()
                .spec(responseSpec);

    }
}