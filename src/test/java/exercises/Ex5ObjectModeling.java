package exercises;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import model.Post;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.post;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Ex5ObjectModeling {
    private static RequestSpecification requestSpec;
    private static ResponseSpecification responseSpec;

    /*************************************************************
     * Request specification should include our base URI
     * and base path pointing to posts collection
     ************************************************************/
    @BeforeAll
    public static void createSpecifications() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri("http://localhost:3000");
        builder.setBasePath("/posts");
        requestSpec = builder.build();
        responseSpec = new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();

    }

    /**********************************************************
     * Check the title of any post using deserialization.
     * You should create a new class Post.java that has the same
     * structure as the objects in the posts collection and
     * has a getter method for the title property.
     *********************************************************/
    @Test
    public void testGetPost() {

        Post myPost = given()
                .spec(requestSpec)
                .pathParam("id", 3)
                .get("/{id}")
                .as(Post.class);
        assertEquals("ea molestias quasi exercitationem repellat qui ipsa sit aut", myPost.getTitle());
        assertEquals("et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut", myPost.getBody());
        assertEquals(1, myPost.getUserId());

    }

    /**********************************************************
     * Add a new item to posts collection using serialization.
     * You should use the new class Post.java - create a new
     * instance of it and set it as the request body.
     * Make sure your Post.java class has a constructor.
     * Check that the response status code is 201 (created).
     *********************************************************/
    @Test
    public void testAddPost() {
        Post newPost= new Post(1, 555, "newTitle", "something new");

        given().contentType("application/json")

                .spec(requestSpec)
                .log().all() .body(newPost)
                .post()
                .then()
                .statusCode(201);






    }
}
